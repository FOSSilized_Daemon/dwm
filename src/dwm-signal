#!/usr/bin/env sh

########################
### Handle Messaging ###
########################
print_error() {
	printf "Error: ${@}\n" >&2
	exit 1
}

#####################
### Handle Checks ###
#####################
check_for_required_dependencies() {
	if ! command -v xsetroot > /dev/null; then
		print_error "xsetroot(1) was not found in PATH."
	fi
}

######################
### Handle Signals ###
######################
send_signal() {
	xsetroot -name "fsignal:${*}"
}

#################################
### Handle Main Functionality ###
#################################
main() {
	case "${#}" in
		1)
			case "${1}" in
				killclient | pushdown | pushup | quit | rioresize | setlayout | tagall | togglebar |togglefullscreen | togglefakefullscreen | togglefloating | view | viewall | zoom)
					send_signal "${1}";;
				*)
					print_error "Unkown command or missing one argument.";;
			esac;;
		2)
			case "${1}" in
				view)
					send_signal "${1}" ui "${2}";;
				cyclelayout | focusmon | focusstack | incnmaster | setgaps | setlayoutex | tagex | tagmon | toggletagex | toggleviewex | viewex)
					send_signal "${1}" i "${2}";;
				setmfact)
					send_signal "${1}" f "${2}";;
				riospawnex | togglescratchex)
					send_signal "${1}" v "${2}";;
				*)
					print_error "Unkown command or missing one argument.";;
			esac;;
		*)
			print_error "Too many arguments.";;
	esac
}

main "${@}"
