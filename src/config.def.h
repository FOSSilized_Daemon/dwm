/*************
 * Apparence *
 *************/
/* Border pixel of windows. */
static const unsigned int borderpx = 1;

/* Enable gaps between windows by default.
 *     0: Disable gaps between windows by default.
 *     1: Enable gaps between windows by default. */
static const int startwithgaps = 1;

/* Default gap size between windows (in pixels). */
static const unsigned int gappx = 10;

/* List of fonts to use for dwm(1) windows, statusbar, etc. */
static const char *fonts[] = { "monospace:size=10" };

/* Colour definitions to be used for colour schemes. */
static const char black[]  = "#000000";
static const char cyan1[]  = "#00ffff";
static const char grey42[] = "#6c6c6c";
static const char red1[]   = "#ff0000";

/* Colour scheme definitions. */
static const char *colors[][3]      = {
	/* Scheme        Foreground    Background    Border */
	[SchemeNorm] = { grey42,       black,        grey42 },
	[SchemeSel]  = { cyan1,        black,        cyan1  },
	[SchemeUrg]  = { red1,         black,        red1   },
};

/*****************
 * Scratchpad(s) *
 *****************/
/* Scratchpad command definitions. */
static const scratchpad scratchpads[] = {
	{ .class = "lisp_repl_term", .v = (char *[]){ "st", "-c", "lisp_repl_term", "-n", "lisp_repl_term", "-g", "34x34", "--", "sbcl", NULL } },
	{ .class = "python_repl_term", .v = (char *[]){ "st", "-c", "python_repl_term", "-n", "python_repl_term", "-g", "34x34", "--", "python3", NULL } },
	{ .class = "misc_term", .v = (char *[]){ "st", "-c", "misc_term", "-n", "misc_term", "-g", "34x34", NULL } },
};

/*****************
 * Functionality *
 *****************/
/* Snap pixel. */
static const unsigned int snap = 32;

/* Enable the statusbar.
 *     0: Disable the statusbar.
 *     1: Enable the statusbar. */
static const int showbar = 1;

/* Location of the statusbar.
 *     0: Show on the bottom of the screen.
 *     1: Show on the top of screen. */
static const int topbar = 1;

/* Enable alternative statusbar program.
 *     0: Use the built-in statusbar.
 *     1: Use an alternative statusbar. */
static const int usealtbar = 1;

/* Alternative statusbar class name. */
static const char *altbarclass = "Polybar";

/* Playbar tray instance name. */
static const char *alttrayname = "tray";

/* Alternative statusbar launch command. */
static const char *altbarcmd = "start-polybar";

/* Enable respecting sizehints.
 *     0: Do not respect sizehints in tiled resizals.
 *     1: Respect sizehints in the tiled resizals. */
static const int resizehints = 0;

/* Swallow floating windows by default.
 *     0: Do not swallow floating windows by default.
 *     1: Swallow floating windows by default. */
static const int swallowfloating = 1;

/* The spawn style of slop(1) (NOTE: Do not define '-f', format, here). */
static const char slopspawnstyle[] = "-t 0 -c 0.92,0.85,0.69,0.3 -o";

/* The resize style of slop(1) (NOTE: Do not define '-f', format, here). */
static const char slopresizestyle[] = "-t 0 -c 0.92,0.85,0.69,0.3";

/* Enable the drawing area of slop(1) also containing the window borders.
 *     0: Do not include window borders.
 *     1: Include window borders. */
static const int riodraw_borders = 0;

/* Application initization in riospawn().
 *     0: Applications are only spawned after a successful selection while.
 *     1: Applications are initialised in the background while the selection is made. */
static const int riodraw_spawnasync = 0;

/* Respect decoration hints (i.e. the_MOTIF_WM_HINTS property).
 *     0: Do not respect decoration hints.
 *     1: Do respect decoration hints. */
static const int decorhints = 1;

/********
 * Tags *
 ********/
/* List of tag names. */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

/* List of rules for windows. */
static const Rule rules[] = {
	/* Get the class, instance, and title of windows using xprop(1). The respective values of each are:
	 *    - WM_CLASS = instance, class
	 *    - WM_NAME  = title */
	/* Class         Instance     Title            Tag(s) Mask  isfloating  isterminal  noswallow    x, y, w, h    Monitor */
	/* Terminal Emulator(s) */
	{ "St",          NULL,        NULL,            0,           0,          1,          0,           -1,-1,-1,-1,  -1 },
	{ "st",          NULL,        NULL,            0,           0,          1,          0,           -1,-1,-1,-1,  -1 },
	{ "st-256color", NULL,        NULL,            0,           0,          1,          0,           -1,-1,-1,-1,  -1 },

	/* Web Browser(s) */
	{ "Firefox",     NULL,        NULL,            1 << 1,      0,          0,          0,           -1,-1,-1,-1,  -1 },
	{ "firefox",     NULL,        NULL,            1 << 1,      0,          0,          0,           -1,-1,-1,-1,  -1 },
	{ "Navigator",   NULL,        NULL,            1 << 1,      0,          0,          0,           -1,-1,-1,-1,  -1 },

	{ "Chromium",    NULL,        NULL,            1 << 1,      0,          0,          0,           -1,-1,-1,-1,  -1 },
	{ "chromium",    NULL,        NULL,            1 << 1,      0,          0,          0,           -1,-1,-1,-1,  -1 },
	{ "Chrome",      NULL,        NULL,            1 << 1,      0,          0,          0,           -1,-1,-1,-1,  -1 },
	{ "chrome",      NULL,        NULL,            1 << 1,      0,          0,          0,           -1,-1,-1,-1,  -1 },

	/* Multimedia Application(s) */
	{ "Blender",     NULL,        NULL,            1 << 2,      0,          0,          0,           -1,-1,-1,-1,  -1 },
	{ "blender",     NULL,        NULL,            1 << 2,      0,          0,          0,           -1,-1,-1,-1,  -1 },

	{ "Handbrake",   NULL,        NULL,            1 << 2,      0,          0,          0,           -1,-1,-1,-1,  -1 },
	{ "handbrake",   NULL,        NULL,            1 << 2,      0,          0,          0,           -1,-1,-1,-1,  -1 },

	{ "Gimp",        NULL,        NULL,            1 << 2,      0,          0,          0,           -1,-1,-1,-1,  -1 },
	{ "gimp",        NULL,        NULL,            1 << 2,      0,          0,          0,           -1,-1,-1,-1,  -1 },
 
	{ "Inksacpe",    NULL,        NULL,            1 << 2,      0,          0,          0,           -1,-1,-1,-1,  -1 },
	{ "inksacpe",    NULL,        NULL,            1 << 2,      0,          0,          0,           -1,-1,-1,-1,  -1 },

	/* Communication and Social Media Application(s) */
	{ "Discord",     NULL,        NULL,            1 << 3,      0,          0,          0,           -1,-1,-1,-1,  -1 },
	{ "discord",     NULL,        NULL,            1 << 3,      0,          0,          0,           -1,-1,-1,-1,  -1 },

	/* Media Application(s) */
	{ "Mpv",         NULL,        NULL,            1 << 4,      0,          0,          0,           -1,-1,-1,-1,  -1 },
	{ "mpv",         NULL,        NULL,            1 << 4,      0,          0,          0,           -1,-1,-1,-1,  -1 },

	{ "Vlc",         NULL,        NULL,            1 << 4,      0,          0,          0,           -1,-1,-1,-1,  -1 },
	{ "vlc",         NULL,        NULL,            1 << 4,      0,          0,          0,           -1,-1,-1,-1,  -1 },

	{ "Steam",       NULL,        NULL,            1 << 4,      0,          0,          0,           -1,-1,-1,-1,  -1 },
	{ "steam",       NULL,        NULL,            1 << 4,      0,          0,          0,           -1,-1,-1,-1,  -1 },

	{ "Lutris",      NULL,        NULL,            1 << 4,      0,          0,          0,           -1,-1,-1,-1,  -1 },
	{ "lutris",      NULL,        NULL,            1 << 4,      0,          0,          0,           -1,-1,-1,-1,  -1 },

	/* Scratchpad(s) */
	{ "lisp_repl_term",   "lisp_repl_term",   NULL,            0,           1,          1,          0,           .35,35,.3,.5, -1 },
	{ "python_repl_term", "python_repl_term", NULL,            0,           1,          1,          0,           .35,35,.3,.5, -1 },
	{ "misc_term",        "misc_term",        NULL,            0,           1,          1,          0,           .35,35,.3,.5, -1 },
};

/***********
 * Layouts *
 ***********/
/* Factor the master area size (how much of the screen the master window takes up).
 *     Ranges: 0.05-0.95 */
static const float mfact = 0.50;

/* Total number of clients in the master area. */
static const int nmaster = 1;

/* List of layouts (NOTE: The first entry is the default layout). The available layouts are as following:
 *    - tile
 *    - monocle
 *    - NULL (this is the floating layout) */
static const Layout layouts[] = {
	/* Symbol     Layout */
	{ "[]=",      tile },
	{ "[M]",      monocle },
	{ "><>",      NULL },
	{ NULL,       NULL },
};

/* List of rules for monitors. */
static const MonitorRule monrules[] = {
	/* Monitor  Tag Mask  Layout  mfact  nmaster  showbar  topbar */
	{  1,       2,        1,      -1,    -1,      0,       -1  },
	{  -1,      -1,       0,      -1,    -1,      -1,      -1  },
};

/*******************
 * Button Bindings *
 *******************/
#define MODKEY Mod4Mask
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }
static const char *termcmd[]  = { "st", NULL };

/* List of button bindings. The following values are available for click:
 *     - ClkTagBar
 *     - ClkLtSymbol
 *     - ClkStatusText
 *     - ClkWinTitle
 *     - ClkClientWin
 *     - ClkRootWin */
static Button buttons[] = {
	/* Click                Event Mask      Button          Function        Argument */
	//{ ClkRootWin,           0,              Button1,        spawn,          { .v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        rioresize,      {0} },
};

/*******
 * IPC *
 *******/
static const char *ipcsockpath = "/tmp/dwm.sock";
static IPCCommand ipccommands[] = {
  IPCCOMMAND(  view,                1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  toggleview,          1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  tag,                 1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  toggletag,           1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  tagmon,              1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  focusmon,            1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  focusstack,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  zoom,                1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  incnmaster,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  killclient,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  togglefloating,      1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  setmfact,            1,      {ARG_TYPE_FLOAT}  ),
  IPCCOMMAND(  setlayoutsafe,       1,      {ARG_TYPE_PTR}    ),
  IPCCOMMAND(  quit,                1,      {ARG_TYPE_NONE}   )
};
