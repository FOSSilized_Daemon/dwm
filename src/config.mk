## dwm(1) version.
VERSION = 6.3

## Installation paths for binaries and man pages.
PREFIX    = /usr/local
MANPREFIX = ${PREFIX}/share/man

## Include paths for X11 libraries.
X11INC = /usr/X11R6/include
X11LIB = /usr/X11R6/lib

## Linker flags for the Xinerama library.
XINERAMALIBS  = -lXinerama
XINERAMAFLAGS = -DXINERAMA

## Linker flags for the Freetype library.
FREETYPELIBS = -lfontconfig -lXft
FREETYPEINC = /usr/include/freetype2

## Linker flags for the YAJL library.
YAJLLIBS = -lyajl
YAJLINC = /usr/include/yajl

## Linker flags for the Freetype library (OpenBSD).
#FREETYPEINC = ${X11INC}/freetype2
#KVMLIB = -lkvm

## Linker flags for the XCB library.
XCBLIBS = -lX11-xcb -lxcb -lxcb-res

## Linker command-line arguments.
INCS = -I${X11INC} -I${FREETYPEINC} -I${YAJLINC}
LIBS = -L${X11LIB} -lX11 ${XINERAMALIBS} ${FREETYPELIBS} ${XCBLIBS} ${KVMLIB} ${YAJLLIBS}

## C++ and C command-line arguments.
CPPFLAGS = -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_POSIX_C_SOURCE=200809L -DVERSION=\"${VERSION}\" ${XINERAMAFLAGS}
#CFLAGS   = -g -std=c99 -pedantic -Wall -O0 ${INCS} ${CPPFLAGS}
CFLAGS   = -std=c99 -pedantic -Wall -Wno-deprecated-declarations -Os ${INCS} ${CPPFLAGS}
LDFLAGS  = ${LIBS}

## C++ and C command-line arguments (Solaris).
#CFLAGS = -fast ${INCS} -DVERSION=\"${VERSION}\"
#LDFLAGS = ${LIBS}

## Compiler.
CC = cc
