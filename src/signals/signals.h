void riospawnex(const Arg *arg);
void setlayoutex(const Arg *arg);
void tagall(const Arg *arg);
void tagex(const Arg *arg);
void togglescratchex(const Arg *arg);
void toggletagex(const Arg *arg);
void toggleviewex(const Arg *arg);
void viewall(const Arg *arg);
void viewex(const Arg *arg);


void
riospawnex(const Arg *arg)
{
	const char *shell_cmd[] = { "sh", "-c", arg->v, NULL };
	riospawn(&((Arg) { .v = shell_cmd }));
}

void
setlayoutex(const Arg *arg)
{
	setlayout(&((Arg) { .v = &layouts[arg->i] }));
}

void
tagall(const Arg *arg)
{
	tag(&((Arg){ .ui = ~0 }));
}

void
tagex(const Arg *arg)
{
	tag(&((Arg) { .ui = 1 << arg->ui }));
}

void
togglescratchex(const Arg *arg)
{
	size_t i;

	for (i = 0; i < LENGTH(scratchpads); i++)
		if ((((char *)scratchpads[i].class != NULL) && strcmp((char *)scratchpads[i].class, (char *)arg->v) == 0) ||
				(((char *)scratchpads[i].title != NULL) && strcmp((char *)scratchpads[i].title, (char *)arg->v) == 0))
			togglescratch(&((Arg) { .v = &(scratchpads[i]) }));
}

void
toggletagex(const Arg *arg)
{
	toggletag(&((Arg) { .ui = 1 << arg->ui }));
}

void
toggleviewex(const Arg *arg)
{
	toggleview(&((Arg) { .ui = 1 << arg->ui }));
}

void
viewall(const Arg *arg)
{
	view(&((Arg){ .ui = ~0 }));
}


void
viewex(const Arg *arg)
{
	view(&((Arg) { .ui = 1 << arg->ui }));
}

static Signal signals[] = {
	/* Signal Number             Function */
	{ "cyclelayout",             cyclelayout },
	{ "focusmon",                focusmon },
	{ "focusstack",              focusstack },
	{ "incnmaster",              incnmaster },
	{ "killclient",              killclient },
	{ "pushup",                  pushup },
	{ "pushdown",                pushdown },
	{ "quit",                    quit },
	{ "rioresize",               rioresize },

	{ "riospawnex",              riospawnex },
	{ "setgaps",                 setgaps },
	{ "setlayout",               setlayout },
	{ "setlayoutex",             setlayoutex },
	{ "setmfact",                setmfact },
	{ "tag",                     tag },
	{ "tagall",                  tagall },
	{ "tagex",                   tagex },
	{ "tagmon",                  tagmon },
	{ "togglescratchex",         togglescratchex },
	{ "toggletag",               tag },
	{ "toggletagex",             toggletagex },
	{ "togglebar",               togglebar },
	{ "togglefullscreen",        togglefullscreen },
	{ "togglefakefullscreen",    togglefakefullscreen },
	{ "togglefloating",          togglefloating },
	{ "toggleview",              view },
	{ "toggleviewex",            toggleviewex },
	{ "view",                    view },
	{ "viewall",                 viewall },
	{ "viewex",                  viewex },
	{ "zoom",                    zoom },

};
